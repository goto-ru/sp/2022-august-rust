# Области в системном программировании


### Общее про компьютерные науки
 - [cs50](https://www.edx.org/cs50)

### Hardware design
 - Программирование на Verilog и VHDL
 - [Курс по FPGA от Влада Алейника](https://github.com/viktor-prutyanov/drec-fpga-intro)
 - STM32
 - [Грант на печать железа от гугла](https://opensource.googleblog.com/2022/05/Build%20Open%20Silicon%20with%20Google.html)
 - RISC-V
 - Реверс железа
 
### Операционные системы
 - [6.S081](https://pdos.csail.mit.edu/6.S081/) про xv6 и прочие основы ОС
 - В Linux Kernel можно контрибьютить "с улицы"
 - Поставить себе Linux и учиться им пользоваться
 - Advanced Level: поставить себе немейнстримный дистрибутив (Alpine, NixOS) и улучшать его инфраструктуру 
 
### Сети
 - Wireguard
 - Писать свой nmap
 - [Computer Networks 5th By Andrew S. Tanenbaum](https://www.mbit.edu.in/wp-content/uploads/2020/05/Computer-Networks-5th-Edition.pdf)
 
### Распределенные системы
 - [Одна из самых цитируемых статей - про Amazon Dynamo](https://www.allthingsdistributed.com/files/amazon-dynamo-sosp2007.pdf)
 
### Компиляторы
 - Доделать kaleidoscope
    - https://github.com/TheDan64/inkwell
 - Писать [Data Parallel C++](https://habr.com/ru/company/intel/blog/533126/)

### Формальная верификация
 - [Coq](https://coq.inria.fr/)
  
 
# Материалы про rust

- [Book of Rust](https://doc.rust-lang.org/book/)

- [Rust Cookbook](https://rust-lang-nursery.github.io/rust-cookbook/)

- [shad-rust](https://gitlab.com/meandrobo/shad-rust)

