# Установка LLVM
Проще всего поставить через llvmenv - утилиту, позволяющую ставить разные версии LLVM и переключаться между ними
```
cargo install llvmenv
llvmenv init
llvmenv entries
llvmenv build-entry 13.0.0
```
