#[allow(unused_variables, unused_assignments)]
fn main() {
    // Можно указать тип и значение при определении
    let logical: bool = true;

    let a_float: f64 = 1.0;  // Regular annotation
    let an_integer   = 5i32; // Suffix annotation

    // Или дать компилятору самому вывести тип
    let default_float   = 3.0; // f64
    let default_integer = 7;   // i32

    // an_integer не mutable, поэтому присвоить другое значение не получится
    // an_integer = 2; // Error: cannot assign twice to immutable variable
    
    // Тип может быть выведен из контекста
    let mut inferred_type = 12; // i64
    inferred_type = 4294967296i64;
    
    // Значения mutable переменных можно менять
    let mut mutable = 12; // Mutable i32
    mutable = 21;
    
    // Error: mismatched types expected integer, found bool
    // mutable = true;
    
    // Переменные можно переопределять, но так делать не стоит
    let mutable = true;

    // В данном случае parse может вывести тип из типа используемой переменной
    let guess: u32 = "42".parse().expect("Not a number!");
    // Но можно указать явно
    let guess2 = "42".parse::<u32>().expect("Not a number!");
}
