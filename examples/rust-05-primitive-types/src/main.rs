#![allow(unused_variables, unused_assignments, overflowing_literals)]

use std::{f64::{INFINITY, NAN}, mem::{size_of, size_of_val}};

fn main() {
    // bool
    let is_raining: bool = true;
    if is_raining {
        println!("Get an umbrella");
    }
    // Логические операции
    println!("true AND false is {}", true && false);
    println!("true OR false is {}", true || false);
    println!("NOT true is {}", !true);
    
    // Целочисленные числа
    // u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, isize, usize
    println!("size_of::<isize>() = {}", size_of::<isize>());
    println!("size_of::<usize>() = {}", size_of::<usize>());

    println!("1 + 2 = {}", 1 + 2);
    println!("1 - 2 = {}", 1 - 2);
    // Error: attempt to compute `1_u32 - 2_u32`, which would overflow
    //println!("1u32 - 2 = {}", 1u32 - 2);
    // Но с  --release или #![allow(overflowing_literals)] компилируется
    println!("1 * 2 = {}", 1 * 2);
    println!("1 / 2 = {}", 1 / 2);
    println!("1 % 2 = {}", 1 % 2);
    // Error: attempt to divide `1_i32` by zero
    // println!("1 / 0 = {}", 1 / 0);

    // Побитовые операции
    println!("0011 AND 0101 is {:04b}", 0b0011u32 & 0b0101);
    println!("0011 OR 0101 is {:04b}", 0b0011u32 | 0b0101);
    println!("0011 XOR 0101 is {:04b}", 0b0011u32 ^ 0b0101);
    println!("1 << 5 is {}", 1u32 << 5);
    println!("0x80 >> 2 is 0x{:x}", 0x80u32 >> 2);

    println!("One million is written as {}", 1_000_000u32);

    // Числа с плавающей точкой
    // f32, f64
    println!("1. / 0. = {}", 1. / 0.);
    println!("inf / 0. = {}", INFINITY / 0.);
    println!("inf * inf = {}", INFINITY * INFINITY);
    println!("inf - inf = {}", INFINITY - INFINITY);
    println!("nan + 1 = {}", NAN + 1.);
    println!("nan == nan = {}", NAN == NAN);
    println!("nan > nan = {}", NAN > NAN);
    println!("nan < nan = {}", NAN < NAN);
    println!("nan != nan = {}", NAN != NAN);

    // char
    println!("size_of::<char>() = {}", size_of::<char>());
    let arr = ['1', 'l', 'ж', '🤓'];
    for character in arr {
        print!("Character {} is ", character);
        if character.is_alphabetic() {
            println!("alphabetical!");
        } else if character.is_numeric() {
            println!("numerical!");
        } else {
            println!("neither alphabetic nor numeric!");
        }
    }

    // Конвертация
    let decimal = 65.4321_f32;

    // Error: mismatched types
    // let integer: u8 = decimal;

    // Явная конвертация
    let integer = decimal as u8;
    let character = integer as char;

    // Error: only u8 can be cast as char, not f32
    // let character = decimal as char;

    println!("Casting: {} -> {} -> {}", decimal, integer, character);

    // При переполнении целочисленных чисел невлезающие биты отбрасываются
    // Если отключить предупреждения о переполнении 
    // 1000 % 256 = 232
    println!("1000 as a u8 is : {}", 1000 as u8);
    // -1 + 256 = 255
    println!("  -1 as a u8 is : {}", (-1i8) as u8);
    // 128 as u8 = -128 as i8
    println!(" 128 as a i8 is : {}", 128 as i8);

    // при переполнении при конвертации float в int, значения приводяться к минимальному/максимальному возможному
    // 300.0 is 255
    println!("300.0 is {}", 300.0_f32 as u8);
    // -100.0 as u8 is 0
    println!("-100.0 as u8 is {}", -100.0_f32 as u8);
    // nan as u8 is 0
    println!("nan as u8 is {}", f32::NAN as u8);
    

    // Кортежи
    let long_tuple = ( 1u8, 2u16, 3u32, 4u64,
                                                        -1i8, -2i16, -3i32, -4i64,
                                                        0.1f32, 0.2f64,
                                                        'a', true);

    println!("long tuple first value: {}", long_tuple.0);
    println!("long tuple second value: {}", long_tuple.1);

    let tuple_of_tuples = ((1u8, 2u16, 2u32), (4u64, -1i8), -2i16);
    println!("tuple of tuples: {:?}", tuple_of_tuples);

    // Кортежи задаются запятой
    println!("one element tuple: {:?}", (5u32,));
    println!("just an integer: {:?}", (5u32));

    // Кортежи можно распаковывать
    let tuple = (1, "hello", 4.5, true);
    let (a, b, c, d) = tuple;
    println!("{:?}, {:?}, {:?}, {:?}", a, b, c, d);


    // Массивы
    // Можно указать все элементы при заполнении
    let xs: [i32; 5] = [1, 2, 3, 4, 5];

    // Или заполнить все элементы одним значением
    let _ys: [i32; 500] = [0; 500];

    // Индексация с 0
    println!("first element of the array: {}", xs[0]);
    println!("second element of the array: {}", xs[1]);

    println!("number of elements in array: {}", xs.len());

    // Массивы создаются на стеке
    println!("array occupies {} bytes", size_of_val(&xs));

    println!("{:?}", xs);

    // Panic: 'index out of bounds: the len is 5 but the index is 5'
    //println!("{}", xs[5]);

    // Слайсы
    // Слайсы указывают на часть массива
    // Они задаются как [starting_index..ending_index]
    // starting_index - индекс первого элемента слайса
    // ending_index - индекс следующего за последним
    let slice: &[i32] = &xs[1 .. 3];
    println!("{:?}", slice);
}
