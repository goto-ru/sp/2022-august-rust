// В Rust нет разницы в каком порядке объявлять функции
fn main() {
    // Можно позвать функцию перед определением
    fizzbuzz_to(100);
}

fn is_divisible_by(lhs: u32, rhs: u32) -> bool {
    // Corner case, early return
    if rhs == 0 {
        return false;
    }

    // Вместо return можно закончить функцию вырожением без ; на конце
    // return lhs % rhs == 0;
    lhs % rhs == 0
}

// Функции, не возвращающие ничего, на самом деле возвращают пустой кортеж
fn fizzbuzz(n: u32) -> () {
    if is_divisible_by(n, 15) {
        println!("fizzbuzz");
    } else if is_divisible_by(n, 3) {
        println!("fizz");
    } else if is_divisible_by(n, 5) {
        println!("buzz");
    } else {
        println!("{}", n);
    }
}

// -> () Можно опустить
fn fizzbuzz_to(n: u32) {
    for n in 1..=n {
        fizzbuzz(n);
    }
}
