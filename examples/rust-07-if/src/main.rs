fn main() {
    let n = 5;

    if n < 0 {
        print!("{} is negative", n);
    } else if n > 0 {
        print!("{} is positive", n);
    } else {
        print!("{} is zero", n);
    }

    let big_n = if n < 10 && n > -10 {
        println!(", and is a small number, increase ten-fold");

        // По аналогии с функциями мы можем вернуть значение из if
        10 * n
    } else {
        println!(", and is a big number, halve the number");

        // Важно, чтобы во всех кейсах тип вовращаемого значения совпадал
        n / 2
    };
//   ^ Точка с запятой для let

    println!("{} -> {}", n, big_n);
}
