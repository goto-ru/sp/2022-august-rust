#![allow(unreachable_code)]

fn main() {
    'outer: loop {
        println!("Entered the outer loop");

        'inner: loop {
            println!("Entered the inner loop");

            // break из внутреннего цикла
            //break;

            // break из внешнего
            break 'outer;
        }

        println!("This point will never be reached");
    }

    println!("Exited the outer loop");

    // Проитерировать от 1 до 10 включительно можно несколькими способами
    let mut n = 1;
    while n < 11 {
        println!("{}", n);
    }

    for n in 1..11 {
        println!("{}", n);
    }

    for n in 1..=10 {
        println!("{}", n);
    }
}
