fn main() {
    // Вектор можно получить, позвав .collect() от итератора
    let collected_iterator: Vec<i32> = (0..10).collect();
    println!("Collected (0..10) into: {:?}", collected_iterator);

    // Или используя макрос vec! с синтаксисом как у массива
    let mut xs = vec![1i32, 2, 3];
    println!("Initial vector: {:?}", xs);

    // Добавление элемента
    println!("Push 4 into the vector");
    xs.push(4);
    println!("Vector: {:?}", xs);

    println!("Vector length: {}", xs.len());
    println!("Second element: {}", xs[1]);

    // pop удаляет и возвращает последний элемент
    println!("Pop last element: {:?}", xs.pop());

    // Выход за границы приводит к панике
    // Panic: index out of bounds: the len is 3 but the index is 3
    //println!("Fourth element: {}", xs[3]);

    // Итерироваться можно по индексам
    println!("Contents of xs:");
    for i in 0..xs.len() {
        println!("> {}", xs[i]);
    }

    // Или с помощью итератора
    println!("Contents of xs:");
    for x in xs.iter() {
        println!("> {}", x);
    }

    // Итераторы в Rust поддерживают множество функций, включая enumerate()
    for (i, x) in xs.iter().enumerate() {
        println!("In position {} we have value {}", i, x);
    }

    // Можно использовать мутабельный итератор
    for x in xs.iter_mut() {
        *x *= 3;
    }
    println!("Updated vector: {:?}", xs);

    // Можно также создать вектор, явно позвав конструктор
    let mut a: Vec<i32> = Vec::new();
    // ещё можно так, но предпочтительней первый вариант
    let mut a = Vec::<i32>::new();
    for i in 0..=20 {
        println!("Lenght = {}", a.len());
        println!("Capacity = {}", a.capacity());
        a.push(i);
    }

}
