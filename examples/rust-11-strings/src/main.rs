fn main() {
    // Ссылка на строку в неизменяемой памяти
    let pangram: &str = "the quick brown fox jumps over the lazy dog";
    println!("Pangram: {}", pangram);

    // Мы можем взять слайс строки так же, как с массивами
    let substr = &pangram[3..];
    println!("Substr: {}", substr);

    // Итерация в обратном порядке без аллокаций
    println!("Words in reverse");
    for word in pangram.split_whitespace().rev() {
        println!("> {}", word);
    }

    // Чтобы работать с каждым символом по отдельности, мы можем сконвертировать их в char
    let mut chars: Vec<char> = pangram.chars().collect();
    chars.sort();
    chars.dedup();

    // а, затем можно сложить в новую пустую строку
    let mut string = String::new();
    for c in chars {
        // Insert a char at the end of string
        string.push(c);
        // Insert a string at the end of string
        string.push_str(", ");
    }

    // Мы можем удалить пробелы и запятые в начале и конце строки
    // результатом является слайс, указывающий на подстроку, поэтому новых аллокаций не требуется
    let chars_to_trim: &[char] = &[' ', ','];
    let trimmed_str: &str = string.trim_matches(chars_to_trim);
    println!("Used characters: {}", trimmed_str);

    // Можно аллоцировать строку на куче
    let alice = String::from("I like dogs");
    // replace, в отличии от trim, не может вернуть подстроку, поэтому аллоцируется новая строка
    let bob: String = alice.replace("dog", "cat");

    println!("Alice says: {}", alice);
    println!("Bob says: {}", bob);

    // Так же можно взять слайс из байтового массива
    let bytes: &[u8; 20] = b"Imma raw binary data";
    // При этом производится рантайм проверка корректности UTF-8 
    let str = std::str::from_utf8(bytes).unwrap();
    println!("{}", str);
}
