#![allow(dead_code)]

#[derive(Debug)]
struct Person {
    name: String,
    age: u8,
}

// A unit struct
// Используется для дженириков
struct Unit;

// A tuple struct
struct Pair(i32, f32);

// Структура с двумя полями
struct Point {
    x: f64,
    y: f64,
}

// Структуры можно использовать как поля в других структурах
struct Rectangle {
    // Прямоугольники можно задавать по двум углам
    top_left: Point,
    bottom_right: Point,
}

fn main() {
    // Если названия переменных совпадают с названиями полей
    // структуры, то можно создать объект так
    let name = String::from("Peter");
    let age = 27;
    let peter = Person { name, age };

    // Печать в debug формате
    println!("{:?}", peter);

    // Инстанцировать Point
    let point: Point = Point { x: 10.3, y: 0.4 };

    // Обращение к полям
    println!("point coordinates: ({}, {})", point.x, point.y);

    // Можно проинициализировать часть полей, а остальные скопировать из другой структуры
    let bottom_right = Point { x: 5.2, ..point };

    // bottom_right.y == point.y
    println!("second point: ({}, {})", bottom_right.x, bottom_right.y);

    // Можно распаковать поля на переменные
    let Point { x: left_edge, y: top_edge } = point;

    let _rectangle = Rectangle {
        top_left: Point { x: left_edge, y: top_edge },
        bottom_right: bottom_right,
    };

    // Instantiate a unit struct
    let _unit = Unit;

    // Instantiate a tuple struct
    let pair = Pair(1, 0.1);

    // У tuple структур доступ к полям как у tuple
    println!("pair contains {:?} and {:?}", pair.0, pair.1);

    // Распаковка tuple структуры
    let Pair(integer, decimal) = pair;

    println!("pair contains {:?} and {:?}", integer, decimal);
}
