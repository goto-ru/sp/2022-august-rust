#[derive(Debug)]
struct Point {
    x: f64,
    y: f64,
}

// Методы и связанные функции задаются в impl блоке структуры
impl Point {
    // Связанные функции привязаны к типу. Они вызываются не от объекта структуры, а от самой структуры
    // Обычно используются в качестве конструктора
    fn origin() -> Point {
        Point { x: 0.0, y: 0.0 }
    }

    fn new(x: f64, y: f64) -> Point {
        Point { x, y }
    }
}

struct Rectangle {
    p1: Point,
    p2: Point,
}

impl Rectangle {
    // Это метод
    // `&self` - это сокращение от `self: &Self`, где `Self` это тип объекта
    // В данном случае `Self` = `Rectangle`
    fn area(&self) -> f64 {
        // `self` - ссылка на объект класса
        let Point { x: x1, y: y1 } = self.p1;
        let Point { x: x2, y: y2 } = self.p2;

        // `abs` - метод `f64`, возвращающий абсолютное значение
        ((x1 - x2) * (y1 - y2)).abs()
    }

    fn perimeter(&self) -> f64 {
        let Point { x: x1, y: y1 } = self.p1;
        let Point { x: x2, y: y2 } = self.p2;

        2.0 * ((x1 - x2).abs() + (y1 - y2).abs())
    }

    // Этот метод требует мутабельный объект
    // `&mut self` эквивалентно `self: &mut Self`
    fn translate(&mut self, x: f64, y: f64) {
        self.p1.x += x;
        self.p2.x += x;

        self.p1.y += y;
        self.p2.y += y;
    }

    fn destroy(self) {
        // Можно разложить self
        let Rectangle { p1, p2 } = self;

        println!("Destroying Rectangle({:?}, {:?})", p1, p2);

        // `first` and `second` go out of scope and get freed
    }
}

fn main() {
    let rectangle = Rectangle {
        // Связанные функции вызываются через ::
        p1: Point::origin(),
        p2: Point::new(3.0, 4.0),
    };

    // Методы зовутся через .
    // `rectangle.perimeter()` эквивалентно `Rectangle::perimeter(&rectangle)`
    println!("Rectangle perimeter: {}", rectangle.perimeter());
    println!("Rectangle area: {}", rectangle.area());

    let mut square = Rectangle {
        p1: Point::origin(),
        p2: Point::new(1.0, 1.0),
    };

    // Cannot borrow `rectangle` as mutable, as it is not declared as mutable
    //rectangle.translate(1.0, 0.0);

    // Звать методы, требующие мутабельной ссылки можно только на мутабельных объектах
    square.translate(1.0, 1.0);


    rectangle.destroy();

    // Error: value used here after moverustcE0382
    //rectangle.destroy();
}
