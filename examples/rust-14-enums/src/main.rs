enum WebEvent {
    // элементы enum могут быть как юнит структуры,
    PageLoad,
    PageUnload,
    // tuple структуры
    KeyPress(char),
    Paste(String),
    // или обычные структуры
    Click { x: i64, y: i64 },
}

// функция принимает WebEvent как аргумент
fn inspect(event: WebEvent) {
    // Распаковка enum с помощью match
    match event {
        WebEvent::PageLoad => println!("page loaded"),
        WebEvent::PageUnload => println!("page unloaded"),
        // Распаковка tuple
        WebEvent::KeyPress(c) => println!("pressed '{}'.", c),
        WebEvent::Paste(s) => println!("pasted \"{}\".", s),
        // Распаковка структур
        WebEvent::Click { x, y } => {
            println!("clicked at x={}, y={}.", x, y);
        },
    }
    // Чтобы не писать WebEvent::, можно использовать
    //use crate::WebEvent::*
}

fn main() {
    let pressed = WebEvent::KeyPress('x');
    let pasted  = WebEvent::Paste("my text".to_owned());
    let click   = WebEvent::Click { x: 20, y: 80 };
    let load    = WebEvent::PageLoad;
    let unload  = WebEvent::PageUnload;

    inspect(pressed);
    inspect(pasted);
    inspect(click);
    inspect(load);
    inspect(unload);
}

