/* enum Option<i32> {
    Some(i32),
    None
}
enum Option<f64> {
    Some(i64),
    None
}
*/

fn checked_division(dividend: i32, divisor: i32) -> Option<i32> {
    if divisor == 0 {
        None
    } else {
        Some(dividend / divisor)
    }
}

fn try_division(dividend: i32, divisor: i32) {
    match checked_division(dividend, divisor) {
        None => println!("{} / {} failed!", dividend, divisor),
        Some(quotient) => {
            println!("{} / {} = {}", dividend, divisor, quotient)
        },
    }
}

fn main() {
    try_division(4, 2);
    try_division(1, 0);

    // При присваивании None нужно явно указывать тип
    let none: Option<i32> = None;
    let _equivalent_none = None::<i32>;

    let optional_float = Some(0f32);

    // unwrap c Some возвращает элементы
    println!("{:?} unwraps to {:?}", optional_float, optional_float.unwrap());

    // unwrap c None приведёт к панике
    //println!("{:?} unwraps to {:?}", none, none.unwrap());


    // Можно распаковывать с помощью match, но выглядит не оч
    let number = Some(7);
    match number {
        Some(i) => {
            println!("Matched `{:?}`", i);
        },
        _ => {},
    };

    // Правильно делать с if let
    if let Some(x) = number {
        println!("Matched {:?}!", x);
    }

    // Можно распечатать вектор в обратном порядке так
    let mut v = vec![1, 2, 3, 4, 5];
    while let Some(x) = v.pop() {
        print!("{} ", x);
    }
    println!("");
}
