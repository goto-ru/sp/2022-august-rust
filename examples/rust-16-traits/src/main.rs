use std::fmt;

pub trait Summary {
    fn summarize(&self) -> String;
}

#[derive(Default)]
pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

#[derive(Default)]
pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

pub fn notify(item: &(impl Summary +?Sized)) {
// pub fn notify(item: &impl Summary )) {
    println!("Breaking news! {}", item.summarize());
}

pub fn notify2<T: Summary>(item: &T) {
    println!("Very breaking news! {}", item.summarize());
}

impl fmt::Display for Tweet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.content)
    }
}
impl Summary for &str {
    fn summarize(&self) -> String {
        format!("Кто-то где-то сказал: {}", self)
    }
}
// Наследование
// Условный

// fn get_fresh_news(from_social: bool) -> Box<impl Summary> {
//     Box::new(if from_social {
//         Tweet {
//             content: "ТРАМП опять президент"
//         }
//     } else {
//         NewsArticle {
//             headline: "Экономике конец",
//             content: "шутка",
//         }
//     })
// }
//
fn get_fresh_news(from_social: bool) -> Box<dyn Summary> {
    if from_social {
        let result: Box<dyn Summary> = Box::new(Tweet {
            content: String::from("ТРАМП опять президент"),
            ..Default::default()
        });
        return result;
    } else {
        return Box::new(NewsArticle {
            headline: String::from("Экономике конец"),
            content: String::from("шутка"),
            ..Default::default()
        })
    }
}

fn main() {
    let article = NewsArticle {
        headline: String::from("Изобретено лекарство от rust, нужно всего лишь..."),
        location: String::from("База Головинка"),
        author: String::from("Верблюд Борис"),
        content: String::from("Буп!"),
    };
    let tweet = Tweet {
        username: String::from("golovinka"),
        content: String::from("возможны осадки в виде головинки"),
        reply: false,
        retweet: false,
    };

    notify(&article);
    println!();
    notify2(&tweet);
    println!();

    println!("{}", tweet);
    notify(&"Привет братцы");
    println!("{}", tweet);
    notify(get_fresh_news(true).as_ref())
}
