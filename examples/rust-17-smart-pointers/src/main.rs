use std::{rc::Rc, cell::Cell, borrow::Borrow};

struct Node {
    x: i32,
    next: Option<Box<Node>>,
}

struct Stack {
    head: Option<Box<Node>>,
}

impl Stack {
    fn new() -> Self {
        Stack{head: None}
    }
    fn push(&mut self, x: i32) {
        self.head = Some(Box::new(Node{x, next: self.head.take()}));
    }
    fn pop(&mut self) -> Option<i32> {
        if let Some(node) = self.head.take() {
            self.head = node.next;
            Some(node.x)
        } else {
            None
        }
    }
}

struct PNode {
    x: i32,
    next: Option<Rc<PNode>>,
}

struct PStack {
    head: Option<Rc<PNode>>,
}

impl PStack {
    fn new() -> Self {
        Self { head: None }
    }
    fn push(&self, x: i32) -> Self {
        let next = if let Some(node) = &self.head {
            Some(node.clone())
        } else {
            None
        };
        Self { head: Some(Rc::new(PNode{x, next})) }
    }
    fn pop(&self) -> (Self, Option<i32>) {
        if let Some(node) = &self.head {
            (Self { head:node.next.clone() }, Some(node.x))
        } else {
            (Self::new(), None)
        }
    }
}

fn main() {
    let mut stack = Stack::new();
    stack.push(1);
    stack.push(2);
    println!("{}", stack.pop().unwrap());
    println!("{}", stack.pop().unwrap());

    let pstack = PStack::new();
    let pstack = pstack.push(1);
    let pstack = pstack.push(2);
    let (_, x) = pstack.pop();
    println!("{}", x.unwrap());
    let (pstack1, x) = pstack.pop();
    println!("{}", x.unwrap());
    let (_, x) = pstack1.pop();
    println!("{}", x.unwrap());

    let obj: Rc<Cell<i32>> = Rc::new(Cell::new(0));
    let obj1 = obj.clone();
    obj1.as_ref().set(1);
    println!("{}", obj.get());
}
