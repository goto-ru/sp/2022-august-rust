#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct list_s {
    struct list_s *next; /* NULL for the last item in a list */
    int data;
} list_t;

/* Inserts a new list item after the one specified as the argument.
 */
list_t *insert_next_to_list(list_t *item, int data) {
    list_t *next = malloc(sizeof(*item));
    next->next = item->next;
    item->next = next;
    item->next->data = data;
    return next;
}

/* Removes an item following the one specificed as the argument.
 */
void remove_next_from_list(list_t *item) {
    free(item->next);
    item->next = item->next->next;
}

/* Returns item data as text.
 */
char *item_data(const list_t *list) {
    char buf[12];

    sprintf(buf, "%d", list->data);
    return buf;
}

int main() {
    const char* s= "👨‍👨‍👦‍👦깍ко̅д👨🏿‍";
    printf("%s", s);
}
