#include <vector>
#include <cstdio>

int main() {
    std::vector<int> arr;
    arr.push_back(1);
    int *ptr = &arr[0];
    arr.push_back(2);
    std::printf("old: %p, new: %p\n", ptr, &arr[0]);
}
