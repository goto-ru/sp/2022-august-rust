## Настройка окружения

1. При работе на Windows рекомендуются работать в [WSL](https://docs.microsoft.com/ru-ru/windows/wsl/install)
1. Склонируйте репозиторий с задачами.

	```
	git clone git@gitlab.com:goto-ru/sp/2022-august-rust.git goto-rust
	```

   Команда `git clone` создаст директорию `goto-rust` и запишет туда все файлы из этого репозитория.
1. Каждый раз перед тем как приступить к решению задач, нужно подтянуть последние версие задач:

	```
	git pull --rebase
	```
1. Установите или обновите Rust, следуя [официальному руководству](https://www.rust-lang.org/tools/install).
1. Установите [VS Code](https://code.visualstudio.com).
1. Установите расширения для VS Code:
   * [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=matklad.rust-analyzer)
   * [CodeLLDB](https://marketplace.visualstudio.com/items?itemName=vadimcn.vscode-lldb)
1. В VS Code откройте директорию, куда вы склонировали репозиторий курса.
    ```
    code goto-rust
    ```


