// Добейтись того, чтобы код компилировался и все тесты проходили, заменив все ???
// Запуск тестов коммандой cargo test или кнопкой Run Tests в VS Code

#[test]
fn unpack_tuple() {
    let cat = ("Furry McFurson", 3.5);
    // Распакуйте кортеж
    ??? = cat;

    assert_eq!(age, 3.5);
    assert_eq!(name, "Furry McFurson");
}

#[test]
fn indexing_tuple() {
    let numbers = (1, 2, 3);
    // Извлеките элемент
    let second = ???;

    assert_eq!(2, second,
        "This is not the 2nd number in the tuple!");
}

#[test]
fn big_array() {
    // Создайте массив длины 100
    let a = ???;
    assert_eq!(a.len(), 100);
}

#[test]
fn slice_out_of_array() {
    let a = [1, 2, 3, 4, 5];
    // Возьмите нужный нужный слайс
    let nice_slice = ???;

    assert_eq!([2, 3, 4], nice_slice);
}
