// Реализуйте функцию square
// Код должен компилироваться
// При запуске cargo run должен выводиться квадрат трёх
// При запуске cargo test, тест должен проходить
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений

fn main() {
    println!("3 * 3 = {}", square(3));
}

#[test]
fn test_square() {
    assert_eq!(square(0), 0);
    assert_eq!(square(-1), 1);
    assert_eq!(square(5), 25);
}
