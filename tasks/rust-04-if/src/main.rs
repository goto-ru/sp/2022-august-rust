#![allow(dead_code)]
// Реализуйте функцию bigger
// При запуске cargo test, тест должен проходить
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений

fn main() {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ten_is_bigger_than_eight() {
        assert_eq!(10, bigger(10, 8));
    }

    #[test]
    fn fortytwo_is_bigger_than_thirtytwo() {
        assert_eq!(42, bigger(32, 42));
    }
}
