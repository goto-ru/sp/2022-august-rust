#![allow(dead_code)]
// Замените ??? на код, удаювающий каждый элемент вектора
// При запуске cargo test, тесты должны проходить
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений

fn main() {}

fn vec_loop(mut v: Vec<i32>) -> Vec<i32> {
    for num in v.iter_mut() {
        ???
    }
    v
}

fn vec_map(v: Vec<i32>) -> Vec<i32> {
    v.iter().map(|num: &i32| {
        ???
    }).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vec_loop() {
        // эквивалентно let v = [2, 4, 6, 8, 10];
        let v: Vec<i32> = (1..).filter(|x| x % 2 == 0).take(5).collect();
        let ans = vec_loop(v.clone());

        assert_eq!(ans, v.iter().map(|x| x * 2).collect::<Vec<i32>>());
    }

    #[test]
    fn test_vec_map() {
        let v: Vec<i32> = (1..).filter(|x| x % 2 == 0).take(5).collect();
        let ans = vec_map(v.clone());

        assert_eq!(ans, v.iter().map(|x| x * 2).collect::<Vec<i32>>());
    }
}
