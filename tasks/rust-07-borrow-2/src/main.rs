// Исправьте ошибки компиляции, добавляя и удаляя & и mut
// При запуске cargo run, программа должна работать
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений

fn main() {
    let fib = vec![1, 1, 2, 3, 5];

    next_fib(fib);

    double(&mut fib);
}

fn next_fib(fib: Vec<i32>) {
    fib.push(fib[fib.len() - 2] + fib[fib.len() - 1]);
}

fn double(mut data: &mut Vec<i32>) {
    data = data.into_iter().map(|x| x * 2).collect();

    println!("{:?}", data);
}
