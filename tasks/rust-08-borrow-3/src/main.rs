// Отразите матрицу по горизантали и вертикали без использования метода reverse
// При запуске cargo run, программа должна работать
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений

fn reverse_matrix(matrix: &mut Vec<Vec<i32>>) {
    ???
}

fn main() {
    let mut matrix = vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
    let mut matrix2 = matrix.clone();
    reverse_matrix(&mut matrix);
    for line in matrix2.iter_mut() {
        line.reverse();
    }
    matrix2.reverse();
    assert_eq!(matrix, matrix2);
}
