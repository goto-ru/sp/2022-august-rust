// Поменяйте две строчки, чтобы исправить ошибки компиляции
// При запуске cargo run, программа должна работать
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений


fn main() {
    let word = current_favorite_color();
    if is_a_color_word(word) { // Поменяйте эту строчку
        println!("That is a color word I know!");
    } else {
        println!("That is not a color word I know.");
    }
}

fn is_a_color_word(attempt: &str) -> bool {
    attempt == "green" || attempt == "blue" || attempt == "red"
}

fn current_favorite_color() -> String {
    "blue" // И эту
}
