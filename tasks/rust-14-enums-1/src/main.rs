// Выполните TODO
// При запуске cargo run, программа должна выполняться
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений

#[derive(Debug)]
enum Message {
    // TODO: определите варианты на основании кода ниже
}

fn main() {
    println!("{:?}", Message::Quit);
    println!("{:?}", Message::Echo);
    println!("{:?}", Message::Move);
    println!("{:?}", Message::ChangeColor);
}
