// Исправьте код
// При запуске cargo run, программа должна выполняться
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений

struct Point {
    x: i32,
    y: i32,
}

fn main() {
    let y: Option<Point> = Some(Point { x: 100, y: 200 });

    match y {
        Some(p) => println!("Co-ordinates are {},{} ", p.x, p.y),
        _ => println!("no match"),
    }
    y; // Не трогайте эту строчку
}
