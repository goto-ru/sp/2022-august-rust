#![allow(dead_code)]
// При запуске cargo test, тесты должны проходить
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений

// Эта функция отказывается генерировать текст для печати на бейджике с именем, если
// вы передаете ей пустую строку. Было бы лучше, если бы она объясниля, в чем проблема,
// вместо того, чтобы просто иногда возвращать None. К счастью, Rust имеет аналогичную
// конструкцию Option, которая может использоваться для выражения причины ошибки. 
// Давайте воспользуемся ей

pub fn generate_nametag_text(name: String) -> Option<String> {
    if name.is_empty() {
        // Empty names aren't allowed.
        None
    } else {
        Some(format!("Hi! My name is {}", name))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn generates_nametag_text_for_a_nonempty_name() {
        assert_eq!(
            generate_nametag_text("Beyoncé".into()),
            Ok("Hi! My name is Beyoncé".into())
        );
    }

    #[test]
    fn explains_why_generating_nametag_text_fails() {
        assert_eq!(
            generate_nametag_text("".into()),
            // Don't change this line
            Err("`name` was empty; it must be nonempty.".into())
        );
    }
}
