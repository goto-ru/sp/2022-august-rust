#![allow(dead_code)]
// При запуске cargo test, тесты должны проходить
// При запуске cargo fmt --check вывод должен быть пустым
// При запуске cargo clippy, не должно быть предупреждений

// Допустим, мы пишем игру, в которой вы можете покупать предметы за жетоны. 
// Все товары стоят 5 жетонов, и всякий раз, когда вы покупаете товары, 
// взимается плата за обработку в размере 1 жетона. Игрок игры вводит, 
// сколько предметов он хочет купить, и функция total_cost рассчитывает общее 
// количество жетонов. Однако, поскольку игрок ввел количество, мы получаем
// его в виде строки, и он мог ввести что угодно, а не только цифры!

// Прямо сейчас эта функция вообще не обрабатывает ошибки (и также неправильно
// обрабатывает случай успеха). Что мы хотим сделать: если мы вызовем функцию 
// parse для строки, которая не является числом, эта функция вернет ParseIntError,
// и в этом случае мы хотим немедленно вернуть эту ошибку из нашей функции, 
// а не пытаться умножать и складывать.

// Есть как минимум два способа это сделать, оба правильные, но один намного короче!

use std::num::ParseIntError;

pub fn total_cost(item_quantity: &str) -> Result<i32, ParseIntError> {
    let processing_fee = 1;
    let cost_per_item = 5;
    let qty = item_quantity.parse::<i32>();

    Ok(qty * cost_per_item + processing_fee)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn item_quantity_is_a_valid_number() {
        assert_eq!(total_cost("34"), Ok(171));
    }

    #[test]
    fn item_quantity_is_an_invalid_number() {
        assert_eq!(
            total_cost("beep boop").unwrap_err().to_string(),
            "invalid digit found in string"
        );
    }
}
