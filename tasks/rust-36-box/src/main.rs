// Во время компиляции Rust должен знать, сколько места занимает тип. Это становится
// проблематичным для рекурсивных типов, где значение может иметь как часть самого
// себя другое значение того же типа. Чтобы обойти эту проблему, мы можем использовать
// Box - умный указатель, используемый для хранения данных в куче.
//
// Рекурсивный тип, который мы реализуем в этом упражнении, - это односвязный список -
// структура данных, часто встречающаяся в функциональных языках программирования.
// Каждый элемент списка содержит два значения: значение текущего элемента и следующий элемент.
// Последний элемент - это значение, называемое Nil
// 
// Шаг 1: используйте Box в определении перечисления, чтобы заставить код компилироваться
// Шаг 2: создайте как пустые, так и непустые списки, заменив todo!()
// Не меняйте тесты

#[derive(PartialEq, Debug)]
pub enum List {
    Cons(i32, List),
    Nil,
}

fn main() {
    println!("This is an empty cons list: {:?}", create_empty_list());
    println!(
        "This is a non-empty cons list: {:?}",
        create_non_empty_list()
    );
}

pub fn create_empty_list() -> List {
    todo!()
}

pub fn create_non_empty_list() -> List {
    todo!()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_empty_list() {
        assert_eq!(List::Nil, create_empty_list())
    }

    #[test]
    fn test_create_non_empty_list() {
        assert_ne!(create_empty_list(), create_non_empty_list())
    }
}