// В этом задании у насс есть вектор u32 "numbers" с значениями
// от 0 до 99 -- [ 0, 1, 2, ..., 98, 99 ]
// Мы хотим одновременно работать с этими числами из 8 потоков
// Каждый поток считает сумму чисел со смещением
// Первый  поток (offset 0), суммирует 0,  8, 16, ...
// Второй  поток (offset 1), суммирует 1,  9, 17, ...
// Третий  поток (offset 2), суммирует 2, 10, 18, ...
// ...
// Восьмой поток (offset 7), суммирует 7, 15, 23, ...

// Поскольку мы используем потоки, все наши структуры должны быть
// thread-safe. Поэтому мы используем Arc.

// Выполните два TODO. Не меняйте другие строчки. Не клонируйте numbers

#![forbid(unused_imports)]
use std::sync::Arc;
use std::thread;

fn main() {
    let numbers: Vec<_> = (0..100u32).collect();
    let shared_numbers = // TODO
    let mut joinhandles = Vec::new();

    for offset in 0..8 {
        let child_numbers = // TODO
        joinhandles.push(thread::spawn(move || {
            let sum: u32 = child_numbers.iter().filter(|n| *n % 8 == offset).sum();
            println!("Sum of offset {} is {}", offset, sum);
        }));
    }
    for handle in joinhandles.into_iter() {
        handle.join().unwrap();
    }
}
